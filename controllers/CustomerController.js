//Import the dependencies
const _ = require('lodash')
const express = require('express');
const { Customer, validate } = require('../model/Customer.model')
//Creating a Router
var router = express.Router();

//get items
router.get('/', async (req, res) => {
    const items = await Customer.find().sort({ name: 1 });
    return res.send(items)
});

router.post('/', async (req, res) => {
    const { error } = validate(req.body)
    if (error) return res.send(error.details[0]
        .message).status(400)

    let item = await Customer.findOne({ name: req.body.name })
    if (user) return res.
        send({ error: 'item already registered' }).status(400)

    item = new Customer(_.pick(req.body, ['name', 'description', 'address', 'hotelId']))
    await item.save()

    return res.send(_.pick(item, ['_id', 'name', 'description', 'address', 'hotelId'])).status(201)
});

module.exports = router;