//Import the dependencies
const _ = require('lodash')
const express = require('express');
const { Hotel, validate } = require('../model/Hotel.model')
//Creating a Router
var router = express.Router();

//get items
router.get('/', async (req, res) => {
    const items = await Hotel.find().sort({ name: 1 });
    return res.send(items)
});

router.post('/', async (req, res) => {
    const { error } = validate(req.body)
    if (error) return res.send(error.details[0]
        .message).status(400)

    let item = await Hotel.findOne({ name: req.body.name })
    if (user) return res.
        send({ error: 'item already registered' }).status(400)

    item = new Hotel(_.pick(req.body, ['name', 'description', 'address']))
    await item.save()

    return res.send(_.pick(item, ['_id', 'name', 'description', 'address'])).status(201)
});

module.exports = router;