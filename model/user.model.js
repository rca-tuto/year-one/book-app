const mongoose = require('mongoose');
const Joi = require('joi')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const config = require('config')
//Attributes of the Course object
var itemSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        maxlength: 255,
        minlength: 3
    },
    email: {
        type: String,
        required: true,
        unique: true,
        maxlength: 255,
        minlength: 3
    },
    password: {
        type: String,
        required: true,
        maxlength: 1024,
        minlength: 3
    },
    isAdmin: {
        type: Boolean,
        default: false,
        required: true
    }
});
itemSchema.methods.generateAuthToken = function () {
    const token = jwt.sign(
        {
            _id: this._id, name: this.name, email: this.email,
            isAdmin: this.isAdmin
        }
        , config.get('jwtPrivateKey'))
    return token
}

const User = mongoose.model('User', itemSchema);

function validateItem(item) {
    const schema = {
        name: Joi.string().max(255).min(3),
        email: Joi.string().max(255).min(3).required().email(),
        password: Joi.string().max(255).min(3).required(),
        isAdmin: Joi.required()
    }
    return Joi.validate(item, schema)
}
module.exports.User = User
module.exports.validate = validateItem