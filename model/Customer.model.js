const mongoose = require('mongoose');
const Joi = require('joi');
const { join } = require('lodash');
//Attributes of the Course object
var itemSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        maxlength: 255,
        minlength: 3
    },
    email: {
        type: String,
        required: true,
        unique: true,
        maxlength: 255,
        minlength: 3
    },
    address: {
        type: String,
        required: true,
        maxlength: 1024,
        minlength: 3
    },
    hotelId: {
        type: mongoose.Types.ObjectId,
        required: true
    }
});

const Customer = mongoose.model('Customer', itemSchema);

function validateItem(item) {
    const schema = {
        name: Joi.string().max(255).min(3),
        hotelId: Joi.object().id,
        email: Joi.string().max(255).min(3).required().email(),
        address: Joi.string().max(255).min(3).required()
    }
    return Joi.validate(item, schema)
}
module.exports.Customer = Customer
module.exports.validate = validateItem